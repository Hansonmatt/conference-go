import requests
import json
from .keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY


def get_pic(city, state):
    # use request to get pic url
    url = "https://api.pexels.com/v1/search"
    headers = {"Authorization": PEXELS_API_KEY}
    params = {
        "query": f"{city} {state}",
        "per_page": 1
    }
    response = requests.get(url, params=params, headers=headers)
    content = json.loads(response.content)

    try:
        return {"picture_url": content["photos"][0]["src"]["original"]}
    except (KeyError, IndexError):
        return {"picture_url": None}

    # create a dictionary of data to use
        # url of pic
    # return the dictioanry


def get_weather(city, state):
    geo_url = "http://api.openweathermap.org/geo/1.0/direct"
    params = {
        "q": f"{city},{state},US",
        "limit": 1,
        "appid": OPEN_WEATHER_API_KEY,
    }
    response = requests.get(geo_url, params=params)
    content = json.loads(response.content)
    lat = content[0]["lat"]
    lon = content[0]["lon"]

    weather_url = "https://api.openweathermap.org/data/2.5/weather"
    coord_d = {
        "appid": OPEN_WEATHER_API_KEY,
        "lat": lat,
        "lon": lon,
        "units": "imperial"
    }

    response = requests.get(weather_url, params=coord_d)
    content = json.loads(response.content)
    weather_d = {
        "temp": content["main"]["temp"],
        "description": content["weather"][0]["description"]
    }
    return weather_d
